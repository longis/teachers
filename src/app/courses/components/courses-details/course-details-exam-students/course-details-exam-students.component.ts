import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingService } from '@universis/common';
import { AdvancedSearchFormComponent, AdvancedTableComponent, AdvancedTableDataResult, AdvancedTableSearchComponent } from '@universis/ngx-tables';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-course-details-exam-students',
  templateUrl: './course-details-exam-students.component.html'
})
export class CourseDetailsExamStudentsComponent implements OnInit, OnDestroy {

  private paramSubscription: Subscription;
  private model: any;
  public courseClassReports: any;
  public data: any;
  public recordsTotal: any;
  public filter = {};
  @Input() searchConfigSrc = 'assets/lists/CourseExams/students.search.json';
  @Input() tableConfigSrc = 'assets/lists/CourseExams/students.config.json';
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;

  constructor(
    private _loadingService: LoadingService,
    private _activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.paramSubscription = this._activatedRoute.params.subscribe(({ courseExam }) => {
      this.model = `instructors/me/exams/${courseExam}/students`;
    });
  }

  onTableLoading({ target = null } = {}) {
    if (target && target.config) {
      target.config.model = this.model;
    }
  }

  onTableDataLoad(data: AdvancedTableDataResult) {
    this._loadingService.hideLoading();
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy() {
    if (this.paramSubscription && !this.paramSubscription.closed) {
      this.paramSubscription.unsubscribe();
    }
  }

}
