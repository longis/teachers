import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {CommonModule, HashLocationStrategy, LocationStrategy} from '@angular/common';

import { ThesesRoutingModule } from './theses-routing.module';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {environment} from '../../environments/environment';
import { ThesesCompletedComponent } from './components/theses-completed/theses-completed.component';
import { ThesesCurrentsComponent } from './components/theses-currents/theses-currents.component';

import {NgPipesModule} from 'ngx-pipes';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '@universis/common';
import {BsDropdownModule} from 'ngx-bootstrap';
import { TeachersSharedModule } from '../teachers-shared/teachers-shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ThesesRoutingModule,
    TranslateModule,
    BsDropdownModule,
    NgPipesModule,
    FormsModule,
    TeachersSharedModule
  ],
  providers: [
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    }
  ],
  declarations: [ThesesCompletedComponent, ThesesCurrentsComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})

export class ThesesModule {
  constructor(private _translateService: TranslateService) {
    environment.languages.forEach((culture) => {
      import(`./i18n/theses.${culture}.json`).then((translations) => {
        this._translateService.setTranslation(culture, translations, true);
      });
    });
  }
}
