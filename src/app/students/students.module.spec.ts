import { StudentsModule } from './students.module';
import { TestBed } from '@angular/core/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('StudentsModule', () => {
  let studentsModule: StudentsModule;

  beforeEach(async() => {
    return TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        TranslateModule.forRoot()
      ]
    }).compileComponents().then( () => {
      const translateService = TestBed.get(TranslateService);
      studentsModule = new StudentsModule(translateService);
    });
  });

  it('should create an instance', () => {
    expect(studentsModule).toBeTruthy();
  });
});
